<?php

/**
 * @file
 * Platform manage code.
 */

/**
 * Menu callback returning controlroom_platform_git_checkout_form().
 */
function controlroom_platform_manage_code($node) {
  $output = drupal_get_form('controlroom_platform_git_checkout_form', $node);
  return $output;
}

/**
 * Form builder for the platform Manage Code administration form.
 */
function controlroom_platform_git_checkout_form($form, $form_state, $node) {
  $publish_path = $node->field_publish_path[LANGUAGE_NONE][0]['value'];
  $exec_path = $publish_path . '/' . $node->title;
  _rg_admin_exec('git fetch origin; git branch -a', $exec_path, $stdout, $stderr);
  // Branches come back like: "bar\n* master\n  zap\n  remotes/origin/master\n
  // remotes/origin/woot\n\n". The * indicates the current branch. We want to
  // split them up and match local and remote branches.
  preg_match_all('/^([ \*]) (remotes\/)?([-\.\/\w\d]*)?$$/m', $stdout, $matches, PREG_SET_ORDER);
  $current = '';
  foreach ($matches as $line) {
    $branch = $line[3];
    // Figure out the active branch.
    if ($line[1] == '*') {
      $current = $line[3];
    }
    // Don't add local branches to the drop down.
    if (!empty($line[2])) {
      $branch = str_replace('origin/', '', $branch);
      $branches[$branch] = $branch;
    }
  }
  $form['platform_branch'] = array(
    '#type' => 'select',
    '#title' => t('Branch'),
    '#options' => $branches,
    '#required' => TRUE,
    '#default_value' => $current,
  );
  $form['platform_gitpull'] = array(
    '#markup' => '<div>' . l(t('git pull'), 'platform/git_pull/' . $node->nid) . '</div>',
  );
  $form['platform_default_branch'] = array(
    '#type' => 'hidden',
    '#value' => $current,
  );
  $form['publish_path'] = array(
    '#type' => 'hidden',
    '#value' => $publish_path . '/' . $node->title,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation handler for controlroom_platform_git_checkout_form().
 */
function controlroom_platform_git_checkout_form_validate($form, $form_state) {
  if ($form_state['values']['platform_branch'] == $form_state['values']['platform_default_branch']) {
    form_set_error('platform_branch', t("You cannot select the current branch - please choose another branch!!"));
  }
}

/**
 * Submit handler for controlroom_platform_git_checkout_form().
 */
function controlroom_platform_git_checkout_form_submit($form, $form_state) {
  $publish_path = $form_state['values']['publish_path'];
  if ($form_state['values']['platform_branch'] != $form_state['values']['platform_default_branch']) {
    // Cd into the directory where the sites exists.
    $commands[] = 'cd ' . $publish_path;
    // Stash the existing changes.
    $commands[] = 'git stash';
    // Checkout the code from the other submitted branch.
    $commands[] = 'git checkout ' . $form_state['values']['platform_branch'];
  }
  controlroom_write_command($commands);
}
