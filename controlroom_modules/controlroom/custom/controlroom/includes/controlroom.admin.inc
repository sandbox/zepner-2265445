<?php

/**
 * @file
 * ControlRoom admin settings.
 */

/**
 * Form builder for the ControlRoom administration form.p
 */
function controlroom_admin_settings_form() {
  $form['environments'] = array(
    '#type' => 'fieldset',
    '#title' => t('Environments'),
  );
  $form['environments']['site_environments'] = array(
    '#type' => 'textfield',
    '#title' => t('Site Environments'),
    '#default_value' => variable_get('site_environments', 'dev,stage,master,live'),
    '#description' => t('Enter a comma separated list of site environments (i.e. dev, stage, upgrade, etc.).'),
    '#required' => TRUE,
  );
  $form['environments']['live_site_environment'] = array(
    '#type' => 'textfield',
    '#title' => t('Live Site Environment'),
    '#default_value' => variable_get('live_site_environment', 'live'),
    '#description' => t('Enter the environment that will be used as the Prelive Site Environment. This cannot be a new environment, it must be in the Site Environments list above (i.e dev, stage, prelive, etc.).'),
    '#required' => TRUE,
  );
  $form['database_credentials'] = array(
    '#type' => 'fieldset',
    '#title' => t('Database Credentials'),
    '#description' => t('Enter the database hostname, username, and password.'),
  );
  $form['database_credentials']['mysql_host'] = array(
    '#type' => 'textfield',
    '#title' => t('Hostname'),
    '#default_value' => variable_get('mysql_host', ''),
    '#required' => TRUE,
  );
  $form['database_credentials']['mysql_username'] = array(
    '#type' => 'textfield',
    '#title' => t('Username'),
    '#default_value' => variable_get('mysql_username', ''),
    '#required' => TRUE,
  );
  $form['database_credentials']['mysql_password'] = array(
    '#type' => 'textfield',
    '#title' => t('Password'),
    '#default_value' => variable_get('mysql_password', ''),
    '#required' => TRUE,
    '#prefix' => '<a onclick="jQuery(\'#passpass\').toggle();jQuery(\'#edit-mysql-password\').val(\'\');">Set Password</a><div id="passpass" style="display:none">',
    '#suffix' => '</div>',
  );
  $form['file_directories'] = array(
    '#type' => 'fieldset',
    '#title' => t('File Directories'),
    '#description' => t('All file paths must be entered exclusive of trailing slashes.'),
  );
  $form['file_directories']['cmd_file_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Command files directory'),
    '#default_value' => variable_get('cmd_file_dir', ''),
    '#description' => t('The path to a directory where ControlRoom\'s <b>file_command.txt</b>, and <b>queue_exec.sh</b> files can be stored. eg. "/home/ctrl/controlroom/util".'),
    '#required' => TRUE,
  );
  $form['file_directories']['vhost_file_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Vhost files directory'),
    '#default_value' => variable_get('vhost_file_dir', ''),
    '#description' => t('The path to a directory where ControlRoom can create vhost files. eg. "/etc/httpd/vhost".'),
    '#required' => TRUE,
  );
  $form['file_directories']['live_db_file_dir'] = array(
    '#type' => 'textfield',
    '#title' => t('Live database files directory'),
    '#default_value' => variable_get('live_db_file_dir', ''),
    '#description' => t('The path to a directory where ControlRoom can store live database files (used to sync the prelive environment with production). eg. "/home/ctrl/controlroom/live_db_files".'),
    '#required' => TRUE,
  );
  return system_settings_form($form);
}

/**
 * Validation handler for controlroom_admin_settings_form().
 */
function controlroom_admin_settings_form_validate($form, $form_state) {
  // Validates that directory paths do not contain trailing slashes.
  $command_file_directory = $form_state['values']['cmd_file_dir'];
  $vhost_file_directory = $form_state['values']['vhost_file_dir'];

  // Command file path validation.
  if ($command_file_directory != rtrim($command_file_directory, '/')) {
    form_set_error('cmd_file_dir', t('Directory paths cannot contain trailing slashes, please remove the slash and re-save.'));
  }
  // Vhost directory path validation.
  if ($vhost_file_directory != rtrim($vhost_file_directory, '/')) {
    form_set_error('vhost_file_dir', t('Directory paths cannot contain trailing slashes, please remove the slash and re-save.'));
  }

  // Validates that the Prelive Site Environment is present in the
  // Site Environments list.
  $environments = $form_state['values']['site_environments'];
  $environments_arr = explode(',', $environments);
  $live_site_environment = $form_state['values']['live_site_environment'];
  if (!in_array($live_site_environment, $environments_arr)) {
    form_set_error('live_site_environment', t('The live-dev environment that you have entered is not present in the Site Environments list. Please enter an existing Site Environment.'));
  }

  // Validates that the necessary file directories exist.
  $command_file_dir_name = $form['file_directories']['cmd_file_dir']['#title'];
  $command_file_dir_path = $form_state['values']['cmd_file_dir'];

  $vhost_file_dir_name = $form['file_directories']['vhost_file_dir']['#title'];
  $vhost_file_dir_path = $form_state['values']['vhost_file_dir'];

  $live_db_file_dir_name = $form['file_directories']['live_db_file_dir']['#title'];
  $live_db_file_dir_path = $form_state['values']['live_db_file_dir'];

  // File directories array keyed by directory name.
  $file_directories = array($command_file_dir_name => $command_file_dir_path, $vhost_file_dir_name => $vhost_file_dir_path,  $live_db_file_dir_name => $live_db_file_dir_path);

  // Machine name field array keyed by field title. This is used to
  // set errors on specific fields.
  $file_dir_machine_name = array($command_file_dir_name => 'cmd_file_dir', $vhost_file_dir_name => 'vhost_file_dir', $live_db_file_dir_name => 'live_db_file_dir');

  // Loop through directory paths to check if the directories exist. If not,
  // issue an error message.
  foreach ($file_directories as $directory_name => $directory_path) {
    if (!file_exists($directory_path)) {
      form_set_error($file_dir_machine_name[$directory_name], t('The @directory_name could not be found. Please ensure that the directory exists, and that you\'ve entered the correct directory path.', array('@directory_name' => $directory_name)));
    }
  }
}
