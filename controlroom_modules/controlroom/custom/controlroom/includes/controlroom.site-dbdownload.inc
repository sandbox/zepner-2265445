<?php

/**
 * @file
 * ControlRoom DB download form.
 */

/**
 * Menu callback returning controlroom_site_db_download_form().
 */
function controlroom_site_db_download($node) {
  $output = drupal_get_form('controlroom_site_db_download_form', $node);
  return $output;
}

/**
 * Form builder for the site database download form.
 */
function controlroom_site_db_download_form($form, $form_state, $node) {
  // Create a list of site environment download source option values.
  $site_environments = variable_get('site_environments', 'dev,stage,master,live');
  $env_exp = explode(',', $site_environments);
  $site_name = controlroom_get_site_name($node->title);
  foreach ($env_exp as $key => $value) {
    $env = $site_name . '.' . $value . '.ctrl.rgnrtr.com';
    $site_environment[$env] = $env;
  }

  $form['controlroom_site_db_download'] = array(
    '#type' => 'select',
    '#title' => t('Database Source'),
    '#description' => t('Select a source to download the database from.'),
    '#options' => $site_environment,
  );
  $form['controlroom_site_db_download_submit'] = array(
    '#type' => 'submit',
    '#value' => t('Download'),
    '#weight' => 15,
    '#submit' => array('controlroom_site_db_download_form_submit'),
  );
  $form['controlroom_site_db_download_node'] = array(
    '#value' => $node,
  );
  return $form;
}

/**
 * Submit handler for controlroom_site_db_download_form().
 *
 * Handles DB exports and SQL file downloads.
 */
function controlroom_site_db_download_form_submit($form, $form_state) {
  global $base_url;
  $platform_node = node_load($form['controlroom_site_db_download_node']['#value']->field_platform[LANGUAGE_NONE][0]['value']);
  $publish_path = $platform_node->field_publish_path[LANGUAGE_NONE][0]['value'];
  $host_platform = $platform_node->title;
  $site_environment = $form_state['values']['controlroom_site_db_download'];
  $db_download_directory = DRUPAL_ROOT . '/sites/default/database_downloads';

  // Create the Database Download diectory if it doesn't exist.
  !is_dir($db_download_directory) ? mkdir($db_download_directory) : '';

  // Construct DB export file paths.
  $sql_file = str_replace('.', '-', $site_environment) . '.sql';
  $unzipped_db_server_file = $db_download_directory . '/' . $sql_file;
  $gzipped_db_server_file = $db_download_directory . '/' . $sql_file . '.gz';
  $gzipped_db_download_file = $base_url . '/sites/default/database_downloads/' . $sql_file . '.gz';

  // Before exporting the DB, delete remnant DB files from the downloads dir.
  $commands[] = 'rm -rf ' . $db_download_directory . '/*';

  // Export the database to the DB download directory.
  $commands[] = 'cd ' . $publish_path . '/' . $host_platform . '/public_html/sites/' . $site_environment;
  $commands[] = 'drush sql-dump --result-file=' . $unzipped_db_server_file . ' --gzip';
  controlroom_execute_database_download_commands($commands);

  // Download the file to the browser.
  if (file_exists($gzipped_db_server_file)) {
    drupal_goto($gzipped_db_download_file);
  }
  else {
    drupal_set_message(t('There was a problem exporting the database file.'), 'error');
  }
}

/**
 * Executes the commands necessary to export database files.
 */
function controlroom_execute_database_download_commands($commands) {
  if (count($commands)) {
    foreach ($commands as $command) {
      exec($command . ' 2>&1', $output, $result);
    }
  }
}
 