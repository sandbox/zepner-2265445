<?php

/**
 * @file
 * ControlRoom manage code.
 */

/**
 * Menu callback returning controlroom_site_source_sync_form().
 */
function controlroom_site_manage_code($node) {
  $output = drupal_get_form('controlroom_site_source_sync_form', $node);
  return $output;
}

/**
 * Form builder for the site Manage Code administration form.
 */
function controlroom_site_source_sync_form($form, $form_state, $node) {
  // Load the platform node to get the publish path.
  $platform_node = node_load($node->field_platform[LANGUAGE_NONE][0]['value']);
  $publish_path = $platform_node->field_publish_path[LANGUAGE_NONE][0]['value'];
  // Get the environment list.
  $site_environments = variable_get('site_environments', 'dev,stage,master,live');
  $env_exp = explode(',', $site_environments);
  $site_name = controlroom_get_site_name($node->title);
  foreach ($env_exp as $key => $value) {
    $site_dir = $site_name . '.' . $value . '.ctrl.rgnrtr.com';
    $exec_path = $publish_path . '/' . $platform_node->title . '/public_html/sites/' . $site_dir;
    _rg_admin_exec('git fetch origin; git remote prune origin; git branch -a', $exec_path, $stdout, $stderr);
    // Branches come back like: "bar\n* master\n  zap\n  remotes/origin/master\n
    // remotes/origin/woot\n\n". The * indicates the current branch. We want to
    // split them up and match local and remote branches.
    preg_match_all('/^([ \*]) (remotes\/)?([-\.\/\w\d]*)?$$/m', $stdout, $matches, PREG_SET_ORDER);
    $current = '';
    foreach ($matches as $line) {
      $branch = $line[3];
      // Figure out the active branch.
      if ($line[1] == '*') {
        $current = $line[3];
      }
      // Don't add local branches to the drop down.
      if (!empty($line[2])) {
        $branch = str_replace('origin/', '', $branch);
        $branches[$branch] = $branch;
      }
    }
    /*if ($current) {
    // I couldn't figure out a better way of determining what remote a branch
    // tracks so lets just pull the config info.
    if (0 != _rg_admin_exec("git config --list",
    $exec_path, $stdout, $stderr)) {
    drupal_set_message(t('Call to git config failed...'), 'error');
    return array();
    }
    preg_match('/^branch\.' . preg_quote($current, '/') .
    '\.remote[ =](\w*)$/m', $stdout, $remote_matches);
    preg_match('/^branch\.' . preg_quote($current, '/') .
    '\.merge[ =]refs\/heads\/(\w*?)$/m', $stdout, $merge_matches);
    if (isset($remote_matches[1]) && isset($merge_matches[1])) {
    $default = $merge_matches[1];
    }
    }*/

    $form['site_environments' . $key] = array(
      '#type' => 'fieldset',
      '#title' => $site_dir,
    );
    $form['site_environments' . $key][$site_name . '_' . $value . '_branch'] = array(
      '#type' => 'select',
      '#title' => t('Branch'),
      '#options' => $branches,
      '#required' => TRUE,
      '#default_value' => $current,
    );
    $form['site_environments' . $key][$site_name . '_' . $value . '_gitpull'] = array(
      '#markup' => l(t('git pull'), 'sites/git_pull/' . $value . '/' . $node->nid . '/' . $platform_node->nid),
    );
    $form[$site_name . '_' . $value . '_default_branch'] = array(
      '#type' => 'hidden',
      '#value' => $current,
    );
    // $alias['@' . $value . '.' . $node->title] = '@' .
    // $value . '.' . $node->title;
  }
  $form['site_name'] = array(
    '#type' => 'hidden',
    '#value' => $site_name,
  );
  $form['publish_path'] = array(
    '#type' => 'hidden',
    '#value' => $publish_path . '/' . $platform_node->title,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Submit handler for controlroom_site_source_sync_form().
 */
function controlroom_site_source_sync_form_submit($form, $form_state) {
  $site_environments = variable_get('site_environments', 'dev,stage,master,live');
  $env_exp = explode(',', $site_environments);
  $site_name = $form_state['values']['site_name'];
  $publish_path = $form_state['values']['publish_path'];

  foreach ($env_exp as $key => $value) {
    // Construct the format with site name, version,
    // environment(dev/stage/qa) and append URL.
    $env = $site_name . '.' . $value . '.ctrl.rgnrtr.com';
    // Check if any of the branches for any of the environments have changed.
    if ($form_state['values'][$site_name . '_' . $value . '_branch'] != $form_state['values'][$site_name . '_' . $value . '_default_branch']) {
      // Cd into the directory where the sites exists.
      $commands[] = 'cd ' . $publish_path . '/public_html/sites/' . $env;
      // Stash the existing changes.
      $commands[] = 'git stash';
      // Checkout the code from the other submitted branch.
      $commands[] = 'git checkout ' . $form_state['values'][$site_name . '_' . $value . '_branch'];
    }
  }
  controlroom_write_command($commands);
}
