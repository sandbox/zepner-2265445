<?php

/**
 * @file
 * ControlRoom file sync form.
 */

/**
 * Menu callback returning the controlroom_site_file_sync_form().
 */
function controlroom_site_manage_files($node) {
  $output = drupal_get_form('controlroom_site_file_sync_form', $node);
  return $output;
}

/**
 * Form builder for the site Manage Files form.
 */
function controlroom_site_file_sync_form($form, $form_state, $node) {
  $platform_node = node_load($node->field_platform[LANGUAGE_NONE][0]['value']);
  $publish_path = $platform_node->field_publish_path[LANGUAGE_NONE][0]['value'];
  $site_environments = variable_get('site_environments', 'dev,stage,master,live');
  $env_exp = explode(',', $site_environments);
  $site_name = controlroom_get_site_name($node->title);
  foreach ($env_exp as $key => $value) {
    // Construct the format with site name, version, environment (dev/stage/qa),
    // and append URL.
    $env = $site_name . '.' . $value . '.ctrl.rgnrtr.com';
    $source_alias[$env] = $env;
    if (strpos($env, '.live.') === FALSE) {
      $dest_alias[$env] = $env;
    }
    // $alias['@' . $value . '.' . $node->title] = '@'
    // . $value . '.' . $node->title;
  }
  $form['file_source'] = array(
    '#type' => 'fieldset',
    '#title' => t('Source'),
    '#collapsible' => TRUE,
  );
  $form['file_source']['file_source_alias'] = array(
    '#type' => 'select',
    '#title' => t('Files source'),
    '#description' => t('Select the source'),
    '#options' => $source_alias,
  );
  $form['file_destination'] = array(
    '#type' => 'fieldset',
    '#title' => t('Destination'),
    '#collapsible' => TRUE,
  );
  $form['file_destination']['file_destination_alias'] = array(
    '#type' => 'select',
    '#title' => t('Files destination'),
    '#description' => t('Select the destination'),
    '#options' => $dest_alias,
  );
  $form['publish_path'] = array(
    '#type' => 'hidden',
    '#value' => $publish_path . '/' . $platform_node->title,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation handler for controlroom_site_file_sync_form().
 */
function controlroom_site_file_sync_form_validate($form, $form_state) {
  $files_source_alias = $form_state['values']['file_source_alias'];
  $files_destination_alias = $form_state['values']['file_destination_alias'];
  // Validate that the source and destination are not the same.
  if ($files_source_alias == $files_destination_alias) {
    form_set_error('file_destination_alias', t('Choose a different destination.'));
  }
}

/**
 * Submit handler for controlroom_site_file_sync_form().
 */
function controlroom_site_file_sync_form_submit($form, $form_state) {
  $publish_path = $form_state['values']['publish_path'];
  $files_source_alias = $form_state['values']['file_source_alias'];
  $files_destination_alias = $form_state['values']['file_destination_alias'];
  $commands[] = 'cd ' . $publish_path . '/public_html/sites';
  // Run "drush rsync" to execute the source and destination file sync.
  $file_sync_drush_cmd = 'sudo rm -rf ' . $files_destination_alias . '/files/*; cp -r ' . $files_source_alias . '/files/* ' . $files_destination_alias . '/files;';
  $commands[] = $file_sync_drush_cmd;
  controlroom_write_command($commands);
}
