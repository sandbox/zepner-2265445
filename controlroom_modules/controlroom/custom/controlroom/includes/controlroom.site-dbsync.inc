<?php

/**
 * @file
 * ControlRoom DB sync form.
 */
 
/**
 * Menu callback returning the database sync form.
 */
function controlroom_site_db_sync($node) {
  $output = drupal_get_form('controlroom_site_db_sync_form', $node);
  return $output;
}

/**
 * Form builder for the database sync form.
 */
function controlroom_site_db_sync_form($form, $form_state, $node) {
  $platform_node = node_load($node->field_platform[LANGUAGE_NONE][0]['value']);
  $publish_path = $platform_node->field_publish_path[LANGUAGE_NONE][0]['value'];
  $site_environments = variable_get('site_environments', 'dev,stage,master,live');
  $env_exp = explode(',', $site_environments);
  $site_name = controlroom_get_site_name($node->title);
  foreach ($env_exp as $key => $value) {
    $env = $site_name . '.' . $value . '.ctrl.rgnrtr.com';
    $source_alias[$env] = $env;
    if (strpos($env, '.live.') === FALSE) {
      $dest_alias[$env] = $env;
    }
    // $alias['@' . $value . '.' . $node->title] = '@' . $value .
    // '.' . $node->title;
  }
  $form['db_source'] = array(
    '#type' => 'fieldset',
    '#title' => t('Source'),
    '#collapsible' => TRUE,
  );
  $form['db_source']['db_source_alias'] = array(
    '#type' => 'select',
    '#title' => t('Database source'),
    '#description' => t('Select the database source.'),
    '#options' => $source_alias,
  );
  $form['db_destination'] = array(
    '#type' => 'fieldset',
    '#title' => t('Destination'),
    '#collapsible' => TRUE,
  );
  $form['db_destination']['db_destination_alias'] = array(
    '#type' => 'select',
    '#title' => t('Database destination'),
    '#description' => t('Select the database destination.'),
    '#options' => $dest_alias,
  );
  $form['db_destination']['db_destination_backup'] = array(
    '#type' => 'checkbox',
    '#title' => t('Backup destination database.'),
    '#description' => t('Backup the destination database before performing the database sync.'),
  );
  $form['live_db_resync'] = array(
    '#type' => 'fieldset',
    '#title' => t('Live Database Resync'),
    '#collapsible' => TRUE,
    '#description' => t('Resync the current live-dev database with the latest live database.'),
  );
  $form['live_db_resync']['resync_pre_live'] = array(
    '#markup' => l(t('Resync live-dev database'), 'ctrl/restore/live/' . $node->nid),
  );
  $form['publish_path'] = array(
    '#type' => 'hidden',
    '#value' => $publish_path . '/' . $platform_node->title,
  );
  $form['submit'] = array(
    '#type' => 'submit',
    '#value' => t('Submit'),
  );
  return $form;
}

/**
 * Validation handler for controlroom_site_db_sync_form().
 */
function controlroom_site_db_sync_form_validate($form, $form_state) {
  // Ensures that the source and destination are not the same.
  $db_source_alias = $form_state['values']['db_source_alias'];
  $db_destination_alias = $form_state['values']['db_destination_alias'];
  if ($db_source_alias == $db_destination_alias) {
    form_set_error('db_destination_alias', t('The database destination can not be the same as the database source - please choose a different destination.'));
  }
}

/**
 * Submit handler for controlroom_site_db_sync_form().
 */
function controlroom_site_db_sync_form_submit($form, &$form_state) {
  $db_source_alias = $form_state['values']['db_source_alias'];
  $db_destination_alias = $form_state['values']['db_destination_alias'];
  $publish_path = $form_state['values']['publish_path'];

  // Backup destination database if Backup DB checkbox is checked - create the
  // database_backups directory if it doesn't exist.
  if ($form_state['values']['db_destination_backup']) {
    $db_backup_filename = str_replace(".ctrl.rgnrtr.com", "-backup.sql", $db_destination_alias);
    $backup_dir = $publish_path . '/database_backups';
    !is_dir($backup_dir) ? mkdir($backup_dir) : '';
    $commands[] = 'drush sql-dump ' . '--result-file=' . $backup_dir . '/' . $db_backup_filename . ' --gzip';
  }

  // Use drush sql-sync to sync up the DB.
  $drush_sql_sync = 'drush sql-sync ' . $db_source_alias . ' ' . $db_destination_alias . ' -y --no-cache';
  $commands[] = 'cd ' . $publish_path . '/public_html/sites';
  $commands[] = $drush_sql_sync;
  controlroom_write_command($commands, 'direct');

  return TRUE;
}
