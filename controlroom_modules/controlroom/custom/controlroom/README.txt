List of variables used in this module.
----------------------------------------
variable_get('site_environments', 'dev,stage,prelive,upgrade') - Provides a list of environments, ex: dev, stage.
variable_get('mysql_username', '') - Mysql database username.
variable_get('mysql_password', '') - Mysql database password.
variable_get('mysql_host', '') - Mysql database host.
variable_get('cmd_file_dir', '') - Directory in which to write executable commands.
